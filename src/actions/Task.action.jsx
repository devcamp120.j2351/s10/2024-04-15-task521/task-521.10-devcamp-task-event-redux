import { ON_BUTTON_CLICK, ON_LIST_ITEM_CLICK, ON_TEXTBOX_CHANGE } from "../constants/Task.constant";

export const onInputChangeAction = (paramValue) => {
    return ({
        type: ON_TEXTBOX_CHANGE,
        payload: paramValue
    })
}

export const onButtonClickAction = () => {
    return ({
        type: ON_BUTTON_CLICK,
    })
}

export const onToggleClickAction = (paramId) =>{
    return ({
        type: ON_LIST_ITEM_CLICK,
        payload: paramId
    })
}