import { Container, Grid, TextField, Button, List, ListItem } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { onInputChangeAction, onButtonClickAction, onToggleClickAction } from "../actions/Task.action";
const TaskComponent = () => {

    const dispatch = useDispatch();

    const { inputTextValue, taskList } = useSelector((reduxData) => {
        console.log(reduxData.TaskReducer);
        return reduxData.TaskReducer;
    })

    const onTextFieldChange = (event) => {
        dispatch(onInputChangeAction(event.target.value));

    }

    const onButtonClick = () => {
        dispatch(onButtonClickAction());
    }

    const onToggleClick = (event) =>{
        dispatch(onToggleClickAction(event.target.id));
    }

    return (
        <Container maxWidth="sm">
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <TextField id="standard-basic" label="Standard" variant="standard" onChange={onTextFieldChange} />
                </Grid>
                <Grid item xs={4}>
                    <Button variant="contained" sx={{ m: 2 }} onClick={onButtonClick}>Add Task</Button>

                </Grid>
                <Grid item xs={12}>
                    <List>
                        {
                            taskList.map((val, key) => {
                                return (
                                    <ListItem 
                                        style = {{color: val.changeColor == true?"red": "green"}} 
                                        onClick = {onToggleClick}
                                        id = {key}
                                        key = {key}
                                        >
                                        {key}. {val.text}
                                    </ListItem>
                                )
                            })
                        }
                    </List>
                </Grid>
            </Grid>
        </Container>
    )
}
export default TaskComponent;