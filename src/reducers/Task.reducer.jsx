import { ON_BUTTON_CLICK, ON_TEXTBOX_CHANGE, ON_LIST_ITEM_CLICK } from "../constants/Task.constant";

const initialState = {
    inputTextValue: "Test",
    taskList: []
};

const TaskReducer = (state = initialState, action) =>{
	// đoạn mã thay đổi state.
    switch (action.type){
        case ON_TEXTBOX_CHANGE: 
            state.inputTextValue = action.payload;
        break;
        case ON_BUTTON_CLICK:
            state.taskList.push({
                text: state.inputTextValue,
                changeColor: true
            });
            state.inputTextValue = "";
        break;
        case ON_LIST_ITEM_CLICK: 
            state.taskList[action.payload].changeColor = !state.taskList[action.payload].changeColor;
            console.log ("State: " + state.taskList[action.payload].changeColor);
        break;
    }
	return {...state};
}

export default TaskReducer;
