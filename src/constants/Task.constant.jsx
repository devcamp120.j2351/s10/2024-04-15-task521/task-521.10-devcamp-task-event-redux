export const ON_TEXTBOX_CHANGE = "Sự kiện thay đổi nội dung ô văn bản";
export const ON_BUTTON_CLICK = "Sự kiện bấm vào nút";
export const ON_LIST_ITEM_CLICK = "Sự kiện bấm vào một item trong list";