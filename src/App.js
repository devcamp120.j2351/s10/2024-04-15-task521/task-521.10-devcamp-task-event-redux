import TaskComponent from "./components/Task.component";


function App() {
  return (
    <div>
      <TaskComponent/>
    </div>
  );
}

export default App;
